/*Programma che mostra il funzionamento di struct e typedef, by RikuTheFuffs
Vuoi accedere ad altro materiale di studio GRATUITO?

- Guarda gli altri tutorial sul mio canale -> https://www.youtube.com/user/RikuTheFuffs
- Seguimi su facebook -> https://www.facebook.com/rikuthefuffspage/
- Guarda i miei corsi su Udemy -> https://www.udemy.com/user/paolo-abela/
- */
#include <iostream>
using namespace std;
/*
dichiaro una struct chiamata "persona" alla quale d'ora in poi potr� fare riferimento
usando il nome "dati" (grazie a typedef)
*/

typedef struct persona
{
	char nome[20];
	char cognome[20];
	int eta;
	float altezza;
} dati;

//"uint" � un tipo nuovo che rappresenter� degli interi senza segno
typedef unsigned int uint;

int main()
{
	//creo delle nuove persone (ognuna di esse conterr� le informazioni definite nella struct)
	dati persone[10];

	//cos� modifico il valore dell'et� di una specifica persona
	persone[0].eta = 120;
	cout << "Eta' persona: " << persone[0].eta << endl;

	//provo ad usare la mia variabile uint forzando un numero negativo
	uint numeroACaso = -100;
	cout << "Numero sballato per via del tipo: " << numeroACaso << endl;

	numeroACaso = 100;
	cout << "Numero corretto: " << numeroACaso << endl;
}
