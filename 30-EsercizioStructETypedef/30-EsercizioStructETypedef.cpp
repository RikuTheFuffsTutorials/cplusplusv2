/*Esercizio su struct e typedef, by RikuTheFuffs
Vuoi accedere ad altro materiale di studio GRATUITO?

- Guarda gli altri tutorial sul mio canale -> https://www.youtube.com/user/RikuTheFuffs
- Seguimi su facebook -> https://www.facebook.com/rikuthefuffspage/
- Guarda i miei corsi su Udemy -> https://www.udemy.com/user/paolo-abela/
- */
#include <iostream>
using namespace std;

typedef struct studente
{
	char nome[20];
	int voto;
} studente;

int main()
{
	studente studenti[5];

	for (int i = 0; i < 5; i++)
	{
		cout << "Inserisci il nome del " << i + 1 << "� studente: ";
		cin >> studenti[i].nome;
		cout << "Inserisci il voto del " << i + 1 << "� studente: ";
		cin >> studenti[i].voto;
	}

	int valoreMinimo = studenti[0].voto;
	int indiceStudenteConVotoMinimo;

	for (int i = 0; i < 5; i++)
	{
		if (valoreMinimo > studenti[i].voto)
		{
			valoreMinimo = studenti[i].voto;
			indiceStudenteConVotoMinimo = i;
		}
	}

	cout << "Studente con voto minimo: "<< studenti[indiceStudenteConVotoMinimo].nome << " con voto: " << studenti[indiceStudenteConVotoMinimo].voto;
}
