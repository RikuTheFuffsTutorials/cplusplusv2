//Vuoi accedere ad altro materiale di studio GRATIS?
//- Iscriviti al canale Youtube -> https://www.youtube.com/user/RikuTheFuffs
//- Seguimi su facebook -> https://www.facebook.com/rikuthefuffspage/
//- Guarda i miei corsi su Udemy -> https://www.udemy.com/user/paolo-abela/
#include <iostream>
#include <conio.h>

#define FRECCIA_SU 72
#define FRECCIA_GIU 80
#define FRECCIA_SINISTRA 75
#define FRECCIA_DESTRA 77

using namespace std;

int main()
{
	bool esci = false;
	while (!esci)
	{
		if (_kbhit()) //se un tasto � stato premuto
		{
			char tastoPremuto = _getch(); //ottenere il codice ASCII del carattere premuto
			//cout << "Hai premuto un tasto! (" << tastoPremuto << ")";
			switch (tastoPremuto)
			{
			case FRECCIA_SU:
				cout << "Vado su";
				esci = true;
				break;
			case FRECCIA_GIU:
				cout << "Vado giu";
				break;
			case FRECCIA_DESTRA:
				cout << "Vado destra";
				break;
			case FRECCIA_SINISTRA:
				cout << "Vado sinistra";
				break;
			default:
				break;
			}
		}
	}
}