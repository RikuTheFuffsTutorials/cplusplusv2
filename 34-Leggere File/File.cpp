/* In questo video vediamo come leggere un file di testo
Vuoi accedere ad altro materiale di studio GRATUITO?
- Guarda gli altri tutorial sul mio canale -> https://www.youtube.com/user/RikuTheFuffs
- Seguimi su Instagram -> @RikuTheFuffs
- Seguimi su facebook -> https://www.facebook.com/rikuthefuffspage/
- Guarda i miei corsi su Udemy -> https://www.udemy.com/user/paolo-abela/
- */

#define _CRT_SECURE_NO_DEPRECATE //questo permette l'utilizzo delle funzioni "non sicure", che normalmente Visual Studio vi impedisce di usare
#include <iostream>
using namespace std;

int fpeek(FILE* file)
{
	int carattere;

	carattere = fgetc(file);
	ungetc(carattere, file);

	return carattere;
}

int main()
{
	FILE* file;
	char bufferDiLettura[100];

	//leggere il file

	//file = fopen("F:\\wa_RikuTheFuffs\\C++\\2.0\\test.txt", "r"); //versione non sicura
	fopen_s(&file, "F:\\wa_RikuTheFuffs\\C++\\2.0\\test.txt", "r"); //versione sicura

	if (!file)
	{
		cout << "Errore nella lettura del file :(";
		return 1;
	}

	cout << "-- File aperto! Ecco il suo contenuto: --" << endl;

	bool stampaAlmenoUnaFrase = false;

	if (fpeek(file) != -1)
	{
		stampaAlmenoUnaFrase = true;
	}

	fgets(bufferDiLettura, 100, file);
	while (!feof(file) || stampaAlmenoUnaFrase)
	{
		stampaAlmenoUnaFrase = false;
		cout << bufferDiLettura;
		fgets(bufferDiLettura, 100, file);
	}
	cout << endl << "-- Fine del file --" << endl;
	fclose(file);
}