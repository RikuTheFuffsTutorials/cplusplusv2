/* In questo video vediamo come creare un programma che dice se il numero inserito � un numero primo
Vuoi accedere ad altro materiale di studio GRATUITO?
- Guarda gli altri tutorial sul mio canale -> https://www.youtube.com/user/RikuTheFuffs
- Seguimi su Instagram -> @RikuTheFuffs
- Guarda i miei corsi su Udemy -> https://www.udemy.com/user/paolo-abela/
- */

#include <iostream>

bool IsPrime(int number)
{
	//un numero � primo se � divisibile per 1 e per s� stesso
	int divider = 2;
	while (divider < number)
	{
		if (number % divider == 0)
		{
			return false;
		}
		divider++;
	}
	return true;
}

int main()
{
	/*

	2 --> primo
	3 --> primo
	4 --> non primo
	*/

	int input;

	std::cout << "Inserisci un numero: ";
	std::cin >> input;

	if (IsPrime(input))
	{
		std::cout << "Il numero " << input << " � primo\n";
	}
	else
	{
		std::cout << "Il numero " << input << " non � primo\n";
	}

	if (IsPrime(2))
	{
		std::cout << "Il numero 2 � primo\n";
	}
	else
	{
		std::cout << "Il numero 2 non � primo\n";
	}

	if (IsPrime(4))
	{
		std::cout << "Il numero 4 � primo\n";
	}
	else
	{
		std::cout << "Il numero 4 non � primo\n";
	}

}