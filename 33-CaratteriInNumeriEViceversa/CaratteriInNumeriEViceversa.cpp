/*Come convertire stringhe in numeri e viceversa, by RikuTheFuffs
Vuoi accedere ad altro materiale di studio GRATUITO?

- Guarda gli altri tutorial sul mio canale -> https://www.youtube.com/user/RikuTheFuffs
- Seguimi su Instagram -> @RikuTheFuffs
- Seguimi su facebook -> https://www.facebook.com/rikuthefuffspage/
- Guarda i miei corsi su Udemy -> https://www.udemy.com/user/paolo-abela/
- */

#include <iostream>
using namespace std;

int main()
{
	//12345 -> "12345"
	char numeroInFormatoStringa[6];
	int numero;
	cin >> numeroInFormatoStringa;
	numero = atoi(numeroInFormatoStringa);

	cout << "Numero in formato numerico + 10: " << (numero + 10)<<endl;
	cout << "Ora inserisci un altro numero:";

	cin >> numero;
	//_itoa_s(numero, numeroInFormatoStringa, 10);
	sprintf_s(numeroInFormatoStringa, "%d", numero);

	cout << "Numero in formato stringa " << numeroInFormatoStringa;
}