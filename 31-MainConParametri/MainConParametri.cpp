/*Come passare parametri alla funzione main(), by RikuTheFuffs
Vuoi accedere ad altro materiale di studio GRATUITO?

- Guarda gli altri tutorial sul mio canale -> https://www.youtube.com/user/RikuTheFuffs
- Seguimi su Instagram -> @RikuTheFuffs
- Seguimi su facebook -> https://www.facebook.com/rikuthefuffspage/
- Guarda i miei corsi su Udemy -> https://www.udemy.com/user/paolo-abela/
- */

#include <iostream>

int main(int numeroParametri, char** parametri)
{
	for (int i = 0; i < numeroParametri; i++)
	{
		std::cout << parametri[i] << std::endl;
	}
}